import java.util.Scanner;
class DecToOthersRecursif{    
    static void printBase(int num, int base) {       
        String digits = "0123456789abcdef";       
        if (num >= base) {          
            printBase(num/base, base);       
        }             
        System.out.println(digits.charAt(num%base));    
    }    
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        
        System.out.println("Masukan nilai Desimal : ");
        String n = in.next();
        System.out.println("Masukan nilai Base : ");
        String b = in.next();

        int num = Integer.parseInt(n);       
        int base = Integer.parseInt(b);       
        printBase(num, base);    
    }     
}
