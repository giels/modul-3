public class FaktorialRekursi {
    static int factorial(int n){
        if(n>1){
            return factorial(n-1)*n;
        }
        return n;
    }
    public static void main(String[] args){
        int n = Integer.parseInt("4");
        System.out.println(factorial(n));
    }
}
