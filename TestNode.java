class TestNode{
    public static void main(String args[]) {
        Node head = new Node();
        head.setData(5);
        head.nextNode = new Node();
        head.nextNode.setData(10);
        head.nextNode.nextNode = null;
        Node currNode = head;
        while (currNode != null) {
            System.out.print(currNode.getData());
            System.out.print(", ");
            currNode = currNode.nextNode;
        }
      }
}
