public class factorialalter {
    static int factorial(int n){
        int result = 1;
        for (int i=n; i>1; i--){
            result *= i;
        }
        return result;
    }
    public static void main(String[] args){
        int n = Integer.parseInt("6");
        System.out.println(factorial(n));
    }
}
